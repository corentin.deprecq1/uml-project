function displayAdvice() {
    let advice = document.getElementById("advice");
    advice.classList.remove("hidden");
}

function hideAdvice(){
    let advice = document.getElementById("advice");
    advice.classList.add("hidden");
}